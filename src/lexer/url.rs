use logos::{Lexer, Logos};
use std::fmt::{Display, Formatter};
use regex::Regex;

/// Tuple struct for link URLs
#[derive(Debug, PartialEq)]
pub struct LinkUrl(String);

/// Implement Display for printing
impl Display for LinkUrl {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// Tuple struct for link texts
#[derive(Debug, PartialEq)]
pub struct LinkText(String);

/// Implement Display for printing
impl Display for LinkText {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

/// Extracts the URL and text from a string that matched a Link token
fn extract_link_info(lex: &mut Lexer<URLToken>) -> (LinkUrl, LinkText) {
    let token = lex.slice();
    let url_regex = Regex::new("(?:href=\")([^\"]*)(?:\")").unwrap();
    let text_regex = Regex::new("(?:>)([^<]*)(?:<)").unwrap();

    let url_capture = url_regex.captures(token);
    let text_capture = text_regex.captures(token);

    let url_as_string = if url_capture.is_some() { url_capture.unwrap().get(1).map_or("", |m| m.as_str()).to_string()} else { String::from("No URL") };
    let text_as_string = if text_capture.is_some() { text_capture.unwrap().get(1).map_or("", |m| m.as_str()).to_string()} else { String::from("No text") };


    return (LinkUrl(url_as_string), LinkText(text_as_string));
}

// Token enum for capturing of link URLs and Texts
#[derive(Logos, Debug, PartialEq)]
pub enum URLToken {
    // TODO: Capture link definitions
    #[regex("<a [^>]*href[^>]*>[^<]*</a\\s*>", extract_link_info)]
    Link((LinkUrl, LinkText)),
    
    #[regex("<[^>]*>", logos::skip)]
    #[regex("[^<]*", logos::skip)]
    #[regex("\n", logos::skip)]
    Ignored,

    // Catch any error
    #[error]
    Error,
}


